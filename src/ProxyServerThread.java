import java.net.*;
import java.io.*;
import java.util.*;

public class ProxyServerThread extends Thread{
	 BufferedReader    rd = null;
	 DataOutputStream out = null;
	 BufferedReader    in = null;
    private Socket socket = null;
    private static final int BUFFER_SIZE = 32768;
    
    public ProxyServerThread(Socket socket){
        super("ProxyThread");
        this.socket = socket;
    }

    public void run() {
        // get input from user
        // send request to server
        // get response from server
        // send response to user
        try{
            out = new DataOutputStream(socket.getOutputStream());
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String inputLine;
            int cnt = 0;
            String urlToCall = "";

            // begin get request from client
            while ((inputLine = in.readLine()) != null) {
            	System.out.println(inputLine);
                try {
                    StringTokenizer tok = new StringTokenizer(inputLine);
                    tok.nextToken();
                }
                catch (Exception e) {
                    break;
                }
                //parse the first line of the request to find the url
                if (cnt == 0) {
                    String[] tokens = inputLine.split(" ");
                    urlToCall = tokens[1];
                    //can redirect this to output log
                    System.out.println("Request for : " + urlToCall);
                }

                cnt++;
            }
            //end get request from client

            //BufferedReader rd = null;
            try {
                //begin send request to server, get response from server
                URL url = new URL(urlToCall);
                URLConnection conn = url.openConnection();
                conn.setDoInput(true);
                //not doing HTTP posts
                conn.setDoOutput(false);
  
                // Get the response
                InputStream is = null;
                //  HttpURLConnection huc = (HttpURLConnection)conn;
                if ( (conn.getContentLength() > 0) || (conn.getContentLength() == -1)) {
                    try {
                        is = conn.getInputStream();
                        rd = new BufferedReader(new InputStreamReader(is));
                    } 
                    catch (IOException ioe) {
                        System.out.println("********* IO EXCEPTION **********: " + ioe);
                    }
                }
                //end send request to server, get response from server

                //begin send response to client
                try {
                	if(is !=  null){
		                byte by[] = new byte[ BUFFER_SIZE ];
		                int index = is.read( by, 0, BUFFER_SIZE );
		                out.writeBytes("HTTP/1.1 200 OK\nContent-Type: "+conn.getContentType()+"\nContent-Length: "+conn.getContentLength()+"\n\n");
		                while ( index != -1 ){
		                  out.write( by, 0, index );
		                 // outputDebug+=by;
		                  index = is.read( by, 0, BUFFER_SIZE );
		                }
		                out.flush();
		                //System.out.println(outputDebug);
                	}
                	else{
                		System.err.println("Input sream is null");
                	}
                }
                catch (Exception e) {
                	System.err.println("cant write to socket");
                	e.printStackTrace();
                }
                finally{
                	closeSockets();
                }

                //end send response to client

            }
            catch (Exception e){
                //can redirect this to error log
                System.err.println("Encountered exception: " + e);
                //encountered error - just send nothing back, so
                //processing can continue
                out.writeBytes("");
            }
            finally{
            	closeSockets();
            }
        
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        finally{
        	closeSockets();
        }
       
    }
    
    void closeSockets(){
    	 //close out all resources
        if (rd != null){
            try{
				rd.close();
			} 
            catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        if (out != null){
            try {
				out.close();
			} 
            catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        if (in != null){
            try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        if (socket != null){
            try{
				socket.close();
			}
            catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
}