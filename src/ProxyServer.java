import java.net.*;
import java.io. *;

public class ProxyServer{
	ProxyServer(int port) throws IOException{
        ServerSocket serverSocket = null;
        boolean listening = true;

        try{
            serverSocket = new ServerSocket(port);
            System.out.println("Started on: " + port);
        } 
        catch (IOException e) {
            System.err.println("Could not listen on port: " + port);
            System.exit(-1);
        }

        //��������� ������������ � ��������� ������
        while (listening){
            new ProxyServerThread(serverSocket.accept()).start();
        }
        serverSocket.close();
    }
}